from drb.exceptions.core import DrbException, DrbFactoryException


class DrbGribNobeException(DrbException):
    pass


class DrbGribNobeFactoryException(DrbFactoryException):
    pass
