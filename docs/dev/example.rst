.. _example:

Examples
=========

Open and read a grib file
-----------------------------
.. literalinclude:: example/open.py
    :language: python
