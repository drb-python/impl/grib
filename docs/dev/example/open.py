import numpy
import xarray
from drb_impl_file import DrbFileFactory

from drb.drivers.grib import DrbGribFactory

path_file = 'file/temperature.grb'

node_file = DrbFileFactory().create(path_file)
node = DrbGribFactory().create(node_file)

# Retrieve a the xarray.DataSet representation of the whole node
numpy_values = node.get_impl(xarray.DataSet)

# Retrieve dimension latitude
node['dimensions']['latitude'].value

# Get the variable 't' (temperature) node.
temp_node = node['t']

# Get a specific attribute of the first group.
temp_node.get_attribute('GRIB_name')

# Get all the attributes
temp_node.attributes

# Retrieve a the numpy representation of the variable temperature
numpy_values = temp_node.get_impl(numpy.ndarray)

# Retrieve a the xarray.DataArray representation of the variable temperature
numpy_values = temp_node.get_impl(xarray.DataArray)
