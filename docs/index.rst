===================
Data Request Broker
===================
---------------------------------
GRIB driver for DRB
--------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-grib/month
    :target: https://pepy.tech/project/drb-driver-grib
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-grib.svg
    :target: https://pypi.org/project/drb-driver-grib/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-grib.svg
    :target: https://pypi.org/project/drb-driver-grib/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-grib.svg
    :target: https://pypi.org/project/drb-driver-grib/
    :alt: Python Version Support Badge

-------------------

This module implements grib format access with DRB data model.
It is able to navigates among the grib contents.

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

