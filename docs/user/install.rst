.. _install:

Installation of zarr implementation
====================================
To include this module into your project, the ``drb-driver-grib`` module shall be referenced into requirements.txt file,
or the following pip line can be run:

.. code-block::

    pip install drb-driver-grib
